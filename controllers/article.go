package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	u "go-gin-blog-webservice/apiHelpers"
	"go-gin-blog-webservice/models"
)

func CreateArticle(c *gin.Context) {

	article := &models.Article{}
	err := json.NewDecoder(c.Request.Body).Decode(article) //decode the request body into struct and failed if any error occur

	if err != nil {
		u.Respond(c.Writer, u.Message(1, "Invalid request"))
		return
	}
	resp := article.Create()
	u.Respond(c.Writer, resp)
}

func ArticleList(c *gin.Context) {
	resp := models.GetAllArticles(c)
	u.Respond(c.Writer, resp)
}

func ArticleDetails(c *gin.Context) {

	article := &models.Article{}
	err := json.NewDecoder(c.Request.Body).Decode(article) //decode the request body into struct and failed if any error occur

	if err != nil {
		u.Respond(c.Writer, u.Message(1, "Invalid request"))
		return
	}
	resp := article.Details()
	u.Respond(c.Writer, resp)

}
