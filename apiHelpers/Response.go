package apiHelpers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func Page(c *gin.Context) int {
	page := c.PostForm("page")
	var p int
	p, err := strconv.Atoi(page)
	if err != nil {
		p = 0
	}
	return p
}

type ResponseData struct {
	Data interface{} `json:"data"`
	Meta interface{} `json:"meta"`
}

func RespondJSON(w *gin.Context, status int, payload interface{}, msg string) {
	fmt.Println("status ", status)

	res := ResponseData{
		Data: payload,
		Meta: map[string]interface{}{
			"status":  status,
			"message": msg,
		},
	}

	w.JSON(200, res)
}

func RespondJSONwithToken(w *gin.Context, status int, payload interface{}, msg string, token string) {
	fmt.Println("status ", status)

	res := ResponseData{
		Data: payload,
		Meta: map[string]interface{}{
			"status":  status,
			"message": msg,
			"token":   token,
		},
	}

	w.JSON(200, res)
}

func RespondPaginateJSON(w *gin.Context, status int, payload interface{}, msg string) {
	fmt.Println("status ", status)

	res := ResponseData{
		Data: payload,
		Meta: map[string]interface{}{
			"status":  status,
			"message": msg,
		},
	}

	w.JSON(200, res)
}

func Message(status int, message string) map[string]interface{} {
	return map[string]interface{}{"status": status, "message": message}
}

func Respond(w http.ResponseWriter, data map[string]interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
