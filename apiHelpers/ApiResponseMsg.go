package apiHelpers

const (
	PaginationLimit  string = "20"
	PaginationOffset string = "0"

	Success = "Success."
	Error   = "Error."
)
