package models

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	u "go-gin-blog-webservice/apiHelpers"
	"go-gin-blog-webservice/resources"
	"gopkg.in/go-playground/validator.v9"
	"strconv"
)

type Article struct {
	gorm.Model
	Nickname string `gorm:"type:varchar(30)"  json:"nickname" validate:"required"`
	Title    string `gorm:"type:varchar(255)" json:"title" validate:"required"`
	Content  string `gorm:"type:text" json:"content" validate:"required"`
}

func (u *Article) TableName() string {
	return "article"
}

var validate *validator.Validate

//Validate incoming article details...
func (article *Article) Validate() (map[string]interface{}, bool) {

	//check for errors and duplicate article

	temp := &Article{}
	err := GetDB().Table("article").Where("title = ?", article.Title).First(temp).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return u.Message(1, "Connection error. Please retry"), false
	}
	if temp.Title != "" {
		return u.Message(1, "Article is already present."), false
	}

	return u.Message(1, "Requirement passed"), true
}

func (article *Article) Create() map[string]interface{} {

	validate = validator.New()
	err := validate.Struct(article)

	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			if err.ActualTag() == "required" {
				response := u.Message(1, err.Field()+" is required.")
				return response
			}
		}
	}
	db.Create(article).Find(article)

	if article.ID != 0 {
		articleResponse := models.ArticleResources{
			ID:        article.ID,
			Nickname:  article.Nickname,
			Title:     article.Title,
			Content:   article.Content,
			CreatedAt: article.CreatedAt,
			UpdatedAt: article.UpdatedAt,
		}
		response := u.Message(0, "Article has been created.")
		response["data"] = articleResponse
		return response
	} else {
		response := u.Message(0, "Connection Error. Unable to create record.")
		return response
	}

}

func GetAllArticles(c *gin.Context) map[string]interface{} {

	var article []models.ArticleResources

	page, _ := strconv.ParseUint(c.PostForm("page"), 10, 64)
	err := GetDB().Table("article").Limit(20).Offset((page - 1) * 20).Order("id desc").Find(&article).Error
	if err != nil {
		response := u.Message(1, "Connection Error. Unable to fetch record.")
		return response
	}

	response := u.Message(0, "Article List")
	response["data"] = article
	return response
}

func (article *Article) Details() map[string]interface{} {

	if article.ID != 0 {
		if result := GetDB().Find(&article); result.Error != nil {
			return u.Message(1, "No records found.")
		}
	} else {
		return u.Message(1, "No records found.")
	}

	articleResponse := models.ArticleResources{
		ID:        article.ID,
		Nickname:  article.Nickname,
		Title:     article.Title,
		Content:   article.Content,
		CreatedAt: article.CreatedAt,
		UpdatedAt: article.UpdatedAt,
	}
	response := u.Message(0, "Article Details")
	response["data"] = articleResponse
	return response

}
