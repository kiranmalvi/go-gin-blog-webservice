package models

import "time"

type ArticleResources struct {
	ID        uint      `json:"id"`
	Nickname  string    `json:"nickname" `
	Title     string    `json:"title"`
	Content   string    `json:"content"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (u *ArticleResources) TableName() string {
	return "article"
}
