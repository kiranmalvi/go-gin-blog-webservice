package Routers

import (
	"github.com/gin-gonic/gin"
	"go-gin-blog-webservice/controllers"
)

func SetupRouter() *gin.Engine {

	r := gin.Default()
	r.Use(func(c *gin.Context) {
		// add header Access-Control-Allow-Origin
		c.Writer.Header().Set("Content-Type", "application/json")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, UPDATE")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-Max")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(200)
		} else {
			c.Next()
		}
	})

	/* api route */
	api := r.Group("/api")
	v1 := api.Group("/v1")
	v1.POST("/article-create", controllers.CreateArticle)
	v1.POST("/article-list", controllers.ArticleList)
	v1.POST("/article-details", controllers.ArticleDetails)

	return r

}
