package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"go-gin-blog-webservice/routers"
	"gopkg.in/go-playground/validator.v8"
	"os"
)

var validate *validator.Validate

func main() {
	e := godotenv.Load()
	if e != nil {
		fmt.Print(e)
	}
	r := Routers.SetupRouter()

	port := os.Getenv("port")
	if port == "" {
		port = "8081" //localhost
	}

	r.Run(":" + port)

}
